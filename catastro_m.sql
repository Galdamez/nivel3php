-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-02-2020 a las 23:51:31
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `catastro_m`
--
CREATE DATABASE IF NOT EXISTS `catastro_m` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `catastro_m`;

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_consultar_empresa` ()  BEGIN
SELECT id_empresa,nombre_em FROM empresas;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_consultar_giros` ()  BEGIN
SELECT * FROM giros;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_consultar_repre` ()  BEGIN
SELECT * FROM tipo_representante;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_insertar_empresa` (IN `pa_nombre_em` VARCHAR(75), IN `pa_nit_em` INT, IN `pa_direccion_em` VARCHAR(300), IN `telefono_em` INT, IN `pa_id_giro` INT)  BEGIN
INSERT empresas(nombre_em,nit_em,direccion_em,telefono_em,id_giro)
VALUES(pa_nombre_em,pa_nit_em,pa_direccion_em,telefono_em,pa_id_giro);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_insertar_repre` (IN `pa_dui_re` INT, IN `pa_nombres_re` VARCHAR(50), IN `pa_apellidos_re` VARCHAR(50), IN `pa_nit_re` INT, IN `pa_direccion_re` VARCHAR(300), IN `pa_telefono_re` INT, IN `pa_id_tipo_representante` INT, IN `pa_id_empresa` INT)  BEGIN
INSERT INTO representantes(dui_re,nombres_re,apellidos_re,nit_re,direccion_re,telefono_re,id_tipo_representante,id_empresa)
VALUES(pa_dui_re,pa_nombres_re,pa_apellidos_re,pa_nit_re,pa_direccion_re,pa_telefono_re,pa_id_tipo_representante,pa_id_empresa);
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_inscrip`
--

CREATE TABLE `detalle_inscrip` (
  `id_detalle` int(11) NOT NULL,
  `num_resolucion` int(11) NOT NULL,
  `id_servicio` int(11) NOT NULL,
  `valor_total` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresas`
--

CREATE TABLE `empresas` (
  `id_empresa` int(11) NOT NULL,
  `nombre_em` varchar(75) COLLATE utf8_spanish_ci NOT NULL,
  `nit_em` int(11) NOT NULL,
  `direccion_em` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `telefono_em` int(11) NOT NULL,
  `id_giro` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `empresas`
--

INSERT INTO `empresas` (`id_empresa`, `nombre_em`, `nit_em`, `direccion_em`, `telefono_em`, `id_giro`) VALUES
(1, 'eeeeeeeee', 9, 'dddddddddddddddddd', 45565656, 1),
(4, 'vvvvvvvvvvvvv', 0, 'grfgfg', 657746, 3),
(7, '7777777777777', 2147483647, 'vvvvvvvvvvvvvvvvvvvvvvvvvvv', 4545, 3),
(9, 'Simas', 4334, 'dfdf', 3434, 1),
(11, 'CMKSC', 128958698, 'KSEJFK FBJ', 349868956, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `giros`
--

CREATE TABLE `giros` (
  `id_giro` int(11) NOT NULL,
  `nombre_giro` varchar(20) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `giros`
--

INSERT INTO `giros` (`id_giro`, `nombre_giro`) VALUES
(1, 'Industrial'),
(2, 'Comercial'),
(3, 'Servicios'),
(4, 'Financiera');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inscripciones`
--

CREATE TABLE `inscripciones` (
  `num_resolucion` int(11) NOT NULL,
  `id_empresa` int(11) NOT NULL,
  `fecha_inscripcion` date NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `representantes`
--

CREATE TABLE `representantes` (
  `dui_re` int(11) NOT NULL,
  `nombres_re` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos_re` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `nit_re` int(11) NOT NULL,
  `direccion_re` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `telefono_re` int(11) NOT NULL,
  `id_tipo_representante` int(11) NOT NULL,
  `id_empresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `representantes`
--

INSERT INTO `representantes` (`dui_re`, `nombres_re`, `apellidos_re`, `nit_re`, `direccion_re`, `telefono_re`, `id_tipo_representante`, `id_empresa`) VALUES
(3434, 'HOLIS', 'HOLOS', 5656, '56VCB', 4545, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id_rol` int(11) NOT NULL,
  `nombre_rol` varchar(25) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id_rol`, `nombre_rol`) VALUES
(1, 'Administrador'),
(2, 'Empleado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios`
--

CREATE TABLE `servicios` (
  `id_servicio` int(11) NOT NULL,
  `nombre_se` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `valor` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `servicios`
--

INSERT INTO `servicios` (`id_servicio`, `nombre_se`, `valor`) VALUES
(1, 'Aseo', 10),
(2, 'Alumbrado publico', 10),
(3, 'Barrido', 8),
(4, 'Acera', 6),
(5, 'Desechos solidos', 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_representante`
--

CREATE TABLE `tipo_representante` (
  `id_tipo_representante` int(11) NOT NULL,
  `tipo_representante` varchar(20) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tipo_representante`
--

INSERT INTO `tipo_representante` (`id_tipo_representante`, `tipo_representante`) VALUES
(1, 'Legal'),
(2, 'Apoderado'),
(3, 'Propietario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `nombre_u` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `apellido_u` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `nom_usuario` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `clave` varchar(75) COLLATE utf8_spanish_ci NOT NULL,
  `id_rol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nombre_u`, `apellido_u`, `correo`, `nom_usuario`, `clave`, `id_rol`) VALUES
(1, 'Erika', 'Galdamez', 'gabrielahello96@gmail.com', 'erika', '202cb962ac59075b964b07152d234b70', 1),
(2, 'Isidro', 'Colocho', 'isidro@gmail.com', 'isidro', '202cb962ac59075b964b07152d234b70', 2),
(3, 'Isaac', 'Montes', 'isaac@gmail.com', 'isa', '202cb962ac59075b964b07152d234b70', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `detalle_inscrip`
--
ALTER TABLE `detalle_inscrip`
  ADD PRIMARY KEY (`id_detalle`),
  ADD KEY `num_resolucion` (`num_resolucion`,`id_servicio`),
  ADD KEY `id_servicio` (`id_servicio`);

--
-- Indices de la tabla `empresas`
--
ALTER TABLE `empresas`
  ADD PRIMARY KEY (`id_empresa`),
  ADD UNIQUE KEY `nit_em` (`nit_em`),
  ADD UNIQUE KEY `telefono_em` (`telefono_em`),
  ADD KEY `id_giro` (`id_giro`);

--
-- Indices de la tabla `giros`
--
ALTER TABLE `giros`
  ADD PRIMARY KEY (`id_giro`);

--
-- Indices de la tabla `inscripciones`
--
ALTER TABLE `inscripciones`
  ADD PRIMARY KEY (`num_resolucion`),
  ADD KEY `id_empresa` (`id_empresa`,`id_usuario`),
  ADD KEY `dui` (`id_usuario`);

--
-- Indices de la tabla `representantes`
--
ALTER TABLE `representantes`
  ADD PRIMARY KEY (`dui_re`),
  ADD UNIQUE KEY `nit_re` (`nit_re`),
  ADD UNIQUE KEY `telefono_re` (`telefono_re`),
  ADD KEY `id_sexo` (`id_tipo_representante`),
  ADD KEY `id_tipo_representante` (`id_tipo_representante`),
  ADD KEY `id_empresa` (`id_empresa`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id_rol`);

--
-- Indices de la tabla `servicios`
--
ALTER TABLE `servicios`
  ADD PRIMARY KEY (`id_servicio`);

--
-- Indices de la tabla `tipo_representante`
--
ALTER TABLE `tipo_representante`
  ADD PRIMARY KEY (`id_tipo_representante`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`),
  ADD UNIQUE KEY `correo` (`correo`),
  ADD UNIQUE KEY `nom_usuario` (`nom_usuario`),
  ADD KEY `id_rol` (`id_rol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `detalle_inscrip`
--
ALTER TABLE `detalle_inscrip`
  MODIFY `id_detalle` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `empresas`
--
ALTER TABLE `empresas`
  MODIFY `id_empresa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `giros`
--
ALTER TABLE `giros`
  MODIFY `id_giro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id_rol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `servicios`
--
ALTER TABLE `servicios`
  MODIFY `id_servicio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `tipo_representante`
--
ALTER TABLE `tipo_representante`
  MODIFY `id_tipo_representante` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=789456124;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `detalle_inscrip`
--
ALTER TABLE `detalle_inscrip`
  ADD CONSTRAINT `detalle_inscrip_ibfk_1` FOREIGN KEY (`num_resolucion`) REFERENCES `inscripciones` (`num_resolucion`),
  ADD CONSTRAINT `detalle_inscrip_ibfk_2` FOREIGN KEY (`id_servicio`) REFERENCES `servicios` (`id_servicio`);

--
-- Filtros para la tabla `empresas`
--
ALTER TABLE `empresas`
  ADD CONSTRAINT `empresas_ibfk_1` FOREIGN KEY (`id_giro`) REFERENCES `giros` (`id_giro`);

--
-- Filtros para la tabla `inscripciones`
--
ALTER TABLE `inscripciones`
  ADD CONSTRAINT `inscripciones_ibfk_1` FOREIGN KEY (`id_empresa`) REFERENCES `empresas` (`id_empresa`),
  ADD CONSTRAINT `inscripciones_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`);

--
-- Filtros para la tabla `representantes`
--
ALTER TABLE `representantes`
  ADD CONSTRAINT `representantes_ibfk_2` FOREIGN KEY (`id_tipo_representante`) REFERENCES `tipo_representante` (`id_tipo_representante`),
  ADD CONSTRAINT `representantes_ibfk_3` FOREIGN KEY (`id_empresa`) REFERENCES `empresas` (`id_empresa`);

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`id_rol`) REFERENCES `roles` (`id_rol`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
