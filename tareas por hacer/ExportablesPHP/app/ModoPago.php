<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Factura;

class ModoPago extends Model
{
	public $timestamps = false;
    protected $table = 'modo_pago';
    protected $fillable = ['num_pago','nombre','otros_detalles'];

    public function facturas(){
    	return $this->hasMany(Factura::class);
    }
}
