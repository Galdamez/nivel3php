<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ModoPago;
use App\Cliente;

class Cartera extends Controller
{
    public function index(){
    	//$modoPago = ModoPago::all();
    	//return $modoPago;
    	return view('pago');
    }


    public function lsClientes(){

    	$clis = Cliente::all();
    	return view('clientes',compact('clis'));
    }

    public function store(Request $imp){
    	ModoPago::create($imp->all());
    	return view('pago');
    }
}
