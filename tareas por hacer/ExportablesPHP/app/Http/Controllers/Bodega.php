<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use PDF;

class Bodega extends Controller
{

    public function index(Request $request)
    {
        $Categorias = Categoria::all();

        return response()->json($Categorais);
    }

    /*
    Metodo: Para exportar pdf utilizando la libreria DOMPDF
    @param: n/a
    @return: Vista de pdf o archivo descargado
    */
    public function downloadPDF() {
        $show = Categoria::find(1);
        $pdf = PDF::loadView('categorias', compact('show'));
        //Si deseo descargar directamente solo cambio el metodo stream por download
        return $pdf->stream('c_productos.pdf');
    }
}
