<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Producto;

class Categoria extends Model
{
	public $timestamps = false;
	protected $table = 'categoria';
	protected $primaryKey='id_categoria';
	protected $fillable=['nombre','descripcion'];

	public function productos(){
		return $this->hasMany(Producto::class,'id_categoria');
	}
}
