<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
	public $timestamps = false;
    protected $table = 'cliente';
    protected $primaryKey ='id_cliente';
    protected $fillable = ['nombre','apellido','direccion','fecha_nacimiento','telefono','email'];
}
