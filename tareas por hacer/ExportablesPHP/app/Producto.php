<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Categoria;

class Producto extends Model
{
	public $timestamps = false;
    protected $table ="producto";
    protected $primaryKey ='id_producto';
	protected $fillable = ['id_producto','nombre','precio','stock','id_categoria'];

	public function categoria(){
		return $this->belongsTo(Categoria::class,'id_categoria');
	}
}
