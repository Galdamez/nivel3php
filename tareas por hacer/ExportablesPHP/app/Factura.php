<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ModoPago;
use App\Cliente;
use App\Detalle;

class Factura extends Model
{
	public $timestamps = false;
	protected $table = 'factura';
	protected $fillable = ['num_factura','codigo','id_cliente','fecha','num_pago'];

	public function modoPago(){
		return $this->belongsTo(ModoPago::class);
	}

	public function cliente(){
		return $this->belongsTo(Cliente::class);
	}

public function detalles(){
		return $this->hasMany(Detalle::class);
	}	    
}
