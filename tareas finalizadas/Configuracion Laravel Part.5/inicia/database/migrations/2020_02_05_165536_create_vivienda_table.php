<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViviendaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vivienda', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('c_habit');
            $table->string('c_banos');
            $table->string('colonia');
            $table->decimal('precio', 8, 2);
            $table->double('tamanio', 8, 2);
            $table->string('municipio');
            $table->string('departamento');
            $table->string('categoria');
            $table->string('negociable');
            $table->string('estado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vivienda');
    }
}
