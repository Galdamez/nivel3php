<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


/*Route::view('/Cliente','clienteV');*/

Route::get('/Cliente','Cliente@index');

Route::get('/Cliente/show','Cliente@show');

Route::get('/casas','Casas@lsVivienda')->name('casas');

Route::get('/nueva','Casas@casas');

Route::post('/insertar','Casas@insertar');