<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Viviendas</title>
	
	<link rel="stylesheet" href="{{('props/bootstrap/css/bootstrap.min.css')}}">
	<!-- <script src="{{asset('components/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script> -->
</head>
<body>
	<div class="container">
		<h1>Nueva Vivienda</h1>
		<form action="insertar" method="POST">
		@csrf
		<div class="row">
			<div class="col">
				<label>Habitaciones:</label>
				<input type="number" name="c_habit" id="c_habit" class="form-control">
			</div>
			<div class="col">
				<label>Baños:</label>
				<input type="number" name="c_banos" id="c_banos" class="form-control">
			</div>
			<div class="col">
				<label>Colonia:</label>
				<input type="text" name="colonia" id="colonia" class="form-control">
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col">
				<label>Precio:</label>
				<input type="text" name="precio" id="precio" class="form-control">
			</div>
			<div class="col">
				<label>Tamaño:</label>
				<input type="text" name="tamanio" id="tamanio" class="form-control">
			</div>
			<div class="col">
				<label>Municipio:</label>
				<input type="text" name="municipio" id="municipio" class="form-control">
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col">
				<label>Departamento:</label>
				<input type="text" name="departamento" id="departamento" class="form-control">
			</div>
			<div class="col">
				<label>Categoria:</label>
				<input type="text" name="categoria" id="categoria" class="form-control">
			</div>
			<div class="col">
				<label>Negociable:</label>
				<input type="text" name="negociable" id="negociable" class="form-control">
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-sm-4">
				<label>Estado:</label>
				<input type="text" name="estado" id="estado" class="form-control">
			</div>
		</div>
		<br>
		<div>
			<input type="submit" value="Guardar" class="btn btn-primary btn-sm">
		</div>
	</form>
	</div>
</body>
</html>