<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Viviendas</title>
	
	<link rel="stylesheet" href="{{('props/bootstrap/css/bootstrap.min.css')}}">
	<!-- <script src="{{asset('components/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script> -->
</head>
<body>
	<div class="container">
		<h1>Viviendas</h1>
		<a href="nueva" class="btn btn-primary btn-sm">Nueva Vivienda</a>
	<table class="table table-hover table-bordered table-striped table-sm">
		<br>
		<br>
		<thead class="thead-dark">
			<tr>
				<th>Habitaciones</th>
				<th>Baños</th>
				<th>Colonia</th>
				<th>Precio</th>
				<th>Tamaño</th>
				<th>Municipio</th>
				<th>Departamento</th>
				<th>Categoria</th>
				<th>Negociable</th>
				<th>Estado</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($vi as $v)
			<tr>
				<td>{{$v->c_habit}}</td>
				<td>{{$v->c_banos}}</td>
				<td>{{$v->colonia}}</td>
				<td>{{$v->precio}}</td>
				<td>{{$v->tamanio}} Mts</td>
				<td>{{$v->municipio}}</td>
				<td>{{$v->departamento}}</td>
				<td>{{$v->categoria}}</td>
				<td>{{$v->negociable}}</td>
				<td>{{$v->estado}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	</div>
</body>
</html>