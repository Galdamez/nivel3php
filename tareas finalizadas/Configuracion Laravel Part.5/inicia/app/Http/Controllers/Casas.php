<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vivienda;

class Casas extends Controller
{
    public function lsVivienda(){

    	$vi = Vivienda::all();
    	return view('vivienda', compact('vi'));
    }

    public function casas(){

    	return view('InsertVivienda');
    }

    public function insertar(Request $imp){


    	Vivienda::create($imp->all());

    	return redirect()->Route('casas');
    }
}
