<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vivienda extends Model
{
    public $timestamps = false;
    protected $table = 'vivienda';
    protected $primaryKey = 'id';
    protected $fillable = ['c_habit','c_banos','colonia','precio','tamanio','municipio','departamento','categoria','negociable','estado'];
}
